@extends('layouts.app')

@section('title')
    Coalition test
@stop

@section('content')


    <products inline-template>

        <div>


            <div class="top-10">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="text-center">Products Management</h3>
                    </div>
                    <div class="col-md-3">
                        <form @submit.prevent="save" id="addForm" class="addForm" action="#" method="post"  role="form">


                            <div class="form-group" :class="{'has-success': !errors.has('name'),'text-danger': errors.has('name')}">
                                <label for="name" class="control-label" :class="{'text-success': !errors.has('name'),'text-danger': errors.has('name')}">Name</label>
                                <input id="name" name="name" type="text" v-model="addProductForm.data.name" class="form-control" :class="{'input': true, 'is-invalid': errors.has('name'), 'is-valid': !errors.has('name') }" v-validate="'required'">
                                <small v-show="errors.has('name')" class="invalid-feedback">@{{ errors.first('name') }}</small>
                            </div>

                            <div class="form-group" :class="{'has-success': !errors.has('quantity'),'text-danger': errors.has('quantity')}">
                                <label for="quantity" class="control-label" :class="{'text-success': !errors.has('quantity'),'text-danger': errors.has('quantity')}">Quantity</label>
                                <input id="quantity" name="quantity" min="0" type="number" step="1" v-model="addProductForm.data.quantity" class="form-control" :class="{'input': true, 'is-invalid': errors.has('quantity'), 'is-valid': !errors.has('quantity') }" v-validate="'required|min_value:1|numeric'">
                                <small v-show="errors.has('quantity')" class="invalid-feedback">@{{ errors.first('quantity') }}</small>
                            </div>


                            <div class="form-group" :class="{'has-success': !errors.has('price'),'text-danger': errors.has('price')}">
                                <label for="price" class="control-label" :class="{'text-success': !errors.has('price'),'text-danger': errors.has('price')}">Price</label>
                                <input id="price" name="price" type="number" step="any" min="0" v-model="addProductForm.data.price" class="form-control" :class="{'input': true, 'is-invalid': errors.has('price'), 'is-valid': !errors.has('price') }" v-validate="'required|min_value:0|numeric'">
                                <small v-show="errors.has('price')" class="invalid-feedback">@{{ errors.first('price') }}</small>
                            </div>

                            <div class="form-group">
                                <button @click='reset' class="btn btn-warning" type="reset" >Reset</button>
                                <button class="btn btn-success" type="submit" >Save</button>
                            </div>

                        </form>
                    </div>
                    <div class="col-md-9"></div>
                </div>


                <div class="table-responsive">
                    <table id="data_tables" class="table table-striped table-bordered" cellspacing="0" width="100%">
                        <thead>
                        <tr>
                            <th>Product name</th>
                            <th>Quantity in stock</th>
                            <th>Price per item</th>
                            <th>Datetime submitted</th>
                            <th>Total value</th>
                            <th class="sorting_disabled"></th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr v-for="(product,index) in products">
                                <td>@{{product.name}}</td>
                                <td>@{{product.quantity}}</td>
                                <td>@{{product.price}}</td>
                                <td>@{{product.created_at}}</td>
                                <td>@{{product.total}}</td>
                                <td>

                                    <div class="btn-group">

                                        <button class="btn-sm btn-danger removeProduct" @click.prevent="destroy(index,product)">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                    </div>

                                </td>
                            </tr>
                        </tbody>
                    </table>

                </div>

            </div>




        </div>

    </products>






@stop

@section('scripts')
    <script>

    </script>
@stop

