<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>@yield('title')</title>

    <meta name="csrf-token" content="{{ csrf_token() }}">


    <link href="{{asset('css/app.css')}}" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="{{asset('css/datatables.min.css')}}"/>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="{{asset('js/jquery.js')}}"></script>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js"></script>


    <!-- Scripts -->
    <script>
        window.Laravel = {
            csrfToken: '{{csrf_token()}}',
            locale: '{{app()->getLocale()}}',
            socket_client: true,
            authenticated: @if(Auth::check()) true @else false @endif,
            user: @if(Auth::check()) JSON.parse(JSON.stringify({!! Auth::user() !!})) @else null @endif,
        };
    </script>

</head>
<body>

<div id="app" v-cloak>

    <div class="container">
        @yield('content')
    </div>

</div>


<script src="{{asset('js/toastr.js')}}"></script>
<script src="{{asset('js/jquery.validate.min.js')}}"></script>
<script src="{{asset('js/datatables.min.js')}}"></script>
<script src="{{mix('js/app.js')}}"></script>

@yield('scripts')

</body>
</html>