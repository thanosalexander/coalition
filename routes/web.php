<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/',[
    'uses'=>'ProductsController@index',
    'as'=>'products.index'
]);

Route::get('products/fetch',[
    'uses'=>'ProductsController@fetch',
    'as'=>'products.fetch'
]);

Route::post('products/store',[
    'uses'=>'ProductsController@store',
    'as'=>'products.store'
]);



Route::delete('products/{product_id}/delete',[
    'uses'=>'ProductsController@delete',
    'as'=>'products.delete'
]);