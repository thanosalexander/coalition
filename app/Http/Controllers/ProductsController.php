<?php

namespace App\Http\Controllers;

use App\Http\Requests\Products\CreateProductRequest;

use App\Http\Requests;
use App\Repositories\XMLProductsRepository;
use Illuminate\Support\Facades\File;

class ProductsController extends Controller
{
    /**
     * @var XMLProductsRepository
     */
    private $productsRepository;

    /**
     * ProductsController constructor.
     * @param XMLProductsRepository $productsRepository
     */
    public function __construct(XMLProductsRepository $productsRepository)
    {
        $this->productsRepository = $productsRepository;
    }

    public function fetch()
    {
        return $this->productsRepository->all();
    }

    public function index()
    {
        $products = $this->productsRepository->all();

        return view('index',compact('products'));
    }

    /**
     * @param CreateProductRequest $request
     * @return \Illuminate\Http\JsonResponse
     * @throws \App\Exceptions\CreateProductException
     */
    public function store(CreateProductRequest $request)
    {
        if(!$request->ajax())  return response()->json([
            'data'=>[],
            'status'=>'Error during saving!'
        ],200);

        $product = $this->productsRepository->create($request);

        $bytes_written = $this->productsRepository->put($product);

        if ($bytes_written === false)
        {
            return response()->json([
                'data'=>[],
                'status'=>'Error during saving!'
            ],200);
        }

        return response()->json([
            'data'=>$product,
            'status'=>'Product added successfully!'
        ],200);
    }



    public function delete($product_id)
    {
        $this->productsRepository->delete($product_id);

        return response()->json([
            'data'=>[],
            'status'=>'Product deleted successfully!'
        ],200);
    }
}
