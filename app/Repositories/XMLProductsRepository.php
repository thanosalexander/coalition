<?php
namespace App\Repositories;


use App\Exceptions\CreateProductException;
use App\Exceptions\DeleteProductException;
use App\Http\Requests\Products\CreateProductRequest;
use Carbon\Carbon;

class XMLProductsRepository
{
    public function all()
    {
        $products = json_decode(file_get_contents(storage_path('database.json')),true);

        return $products;
    }

    /**
     * Creates a product
     *
     * @param CreateProductRequest $request
     * @return mixed
     * @throws CreateProductException
     */
    public function create(CreateProductRequest $request)
    {
        try {

            $product = [
                'id'=>$this->nextId(),
                'name'=>$request->get('name'),
                'quantity'=>$request->get('quantity'),
                'price'=>$request->get('price'),
                'created_at'=>Carbon::now()->format('d/m/Y'),
                'total'=>$request->get('quantity')*$request->get('price')
            ];

            return $product;

        } catch (\Exception $e)
        {
            throw new CreateProductException;
        }
    }


    /**
     * Deletes a product
     *
     * @param $product_id
     * @return mixed
     * @throws DeleteProductException
     */
    public function delete($product_id)
    {
        try {

            $products = collect(json_decode(file_get_contents(storage_path('database.json')),true));

            $products= $products->filter(function ($product)use($product_id){

                return $product['id']!=$product_id;
            });

            file_put_contents(storage_path('database.json'), json_encode($products));

        } catch (\Exception $e)
        {
            throw new DeleteProductException;
        }
    }

    public function put($data)
    {
        $products = json_decode(file_get_contents(storage_path('database.json')),true);

        $products[] = $data;

        return file_put_contents(storage_path('database.json'), json_encode($products));
    }

    public function nextId()
    {
        $products = json_decode(file_get_contents(storage_path('database.json')),true);

        return count($products)+1;
    }
}